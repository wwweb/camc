// Mixing jQuery and Node.js code in the same file? Yes please!

var os = require('os');
var fs = require('fs');
var gui = require('nw.gui');
var child_process = require('child_process');
var pretty_seconds = require('pretty-seconds');
var ssp = require('ssp');

var win = gui.Window.get();
var SECONDS = 0;
var TIMEOUT;

var notes = {
	1 : "10 РУБ",
	2 : "50 РУБ",
	3 : "100 РУБ",
	4 : "500 РУБ",
	5 : "1000 РУБ",
	6 : "5000 РУБ"
};
var seconds = {
	1 : 60,
	2 : 60*5,
	3 : 60*10,
	4 : 60*50,
	5 : 60*100,
	6 : 60*500
};

gui.Screen.Init();

$.noty.defaults = {
    layout: 'bottomCenter',
    theme: 'simple', // or 'relax'
    type: 'information',
    text: '', // can be html or string
    dismissQueue: true, // If you want to use queue feature set this true
    template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
    animation: {
        open: 'animated wobble', // or Animate.css class names like: 'animated bounceInLeft'
        close: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceOutLeft'
        easing: 'swing',
        speed: 600 // opening & closing animation speed
    },
    timeout: false, // delay for closing event. Set false for sticky notifications
    force: false, // adds notification to the beginning of queue when set to true
    modal: false,
    maxVisible: 3, // you can set max visible notification for dismissQueue true option,
    killer: false, // for close all notifications before show
    closeWith: ['click'], // ['click', 'button', 'hover', 'backdrop'] // backdrop click will close all notifications
    callback: {
        onShow: function() {
			$.playSound('./notice.wav');
			setTimeout(function() {
				$('audio').remove();
			}, 7000);
		},
        afterShow: function() {},
        onClose: function() {},
        afterClose: function() {},
        onCloseClick: function() {},
    },
    buttons: false // an array of buttons
};

var RESET_HOTKEY = new gui.Shortcut({
	key : 'Ctrl+Alt+4',
	active : function() {
		console.log("System restart");
		add_time(5);
		restart_system(function() {
			child_process.exec('cmd /c taskkill HyperLaunch.exe');
			child_process.execFile('C:\\HyperSpin\\HyperSpin.exe');
		});
	},
	failed : function(msg) {
		console.log(msg);
	}
});
gui.App.registerGlobalHotKey(RESET_HOTKEY);

win.on('close', function() {
	ssp.disable();
	unlock_interface();
	// Unregister the global desktop shortcut.
	gui.App.unregisterGlobalHotKey(RESET_HOTKEY);
	win.close(1);
});

// двойной клик по окну симулирует внесение купюры
$(document).on('dblclick', function() {
	add_credit(1);
});

win.showDevTools();

function lock_interface(say_bye) {
	if(say_bye) {
		$('#bye').show();
		setTimeout(function() {
			$('#bye').hide();
			$('#insert_coin').show();
		}, 7000);
	}
	else {
		$('#insert_coin').show();
	}
	// убрать все уведомления
	$.noty.closeAll();
	setTimeout(function() {
		restart_system();
	}, 3000);
	// БЛОКИРОВКА КЛАВЫ ДОЛЖНА ЗАПУСКАТЬСЯ ПОСЛЕ ЗАПУСКА HYPERSPIN,
	// ИНАЧЕ КЛАВА НЕ ЗАБЛОКИРУЕТСЯ И WINDOWS БУДЕТ ЖЕСТОКО ГЛЮЧИТЬ
	setTimeout(function() {
		// этот скрипт запускает HyperSpin, ждет его, а потом блокирует клаву
		child_process.exec('lock_keyboard.exe');
	}, 7000);
}

function unlock_interface() {
	$('#insert_coin, #bye').hide();
	// убиваем блокировку клавы
	child_process.exec('cmd /c taskkill /im lock_keyboard.exe /f', function() {
		// очистка иконок в трее
		child_process.execFile('str.exe');
	});
}

function restart_system(callback) {
	// bat-файл убивает все эмуляторы и перезапускает HyperSpin
	// exec ничего не возвращает, колбэки не работают
	child_process.exec('cmd /c reset_hyperspin.bat', callback);
}

function timer_notice(seconds) {
	if(!seconds) return;
	var options = {
		text : 'Осталось ' + pretty_seconds(seconds),
		timeout : 3500
	};
	if(seconds == 10) noty(options); // за 10 сек до окончания
	else if(seconds <= 120 && seconds % 30 == 0) noty(options); // раз в 30 сек, если остаток 2 мин. или менее
	else if(seconds <= 1800 && seconds % 300 == 0) noty(options); // раз в 5 минут, если остаток 30 мин. или менее
	else if(seconds > 1800 && seconds % 600 == 0) noty(options); // раз в 10 минут, если остаток более 30 мин.
}

setInterval(function() {
	// восстановление размеров окна
	var screen_width = gui.Screen.screens[0].bounds.width;
	var screen_height = gui.Screen.screens[0].bounds.height;
	win.resizeTo(screen_width, screen_height);
	win.moveTo(0, 0);
	win.setAlwaysOnTop(true);
	// вызов таймера
	if(SECONDS > 0) {
		timer_notice(SECONDS);
		SECONDS--;
	}
}, 1000);

function add_time(seconds) {
	seconds += 3;
	SECONDS += seconds;
	unlock_interface();
	if(TIMEOUT) clearTimeout(TIMEOUT);
	TIMEOUT = setTimeout(function() {
		SECONDS = 3;
		lock_interface(true);
	}, SECONDS * 1000);
}

function add_credit(note) {
	noty({
	  text: 'ПРИНЯТО ' + notes[note] + '<br>' + 'ДОБАВЛЕНО ' + pretty_seconds(seconds[note]),
	  timeout: 3500,
	  type: 'success'
	});
	add_time(seconds[note]);
	ssp_log('CREDIT ' + notes[note]);
}

function ssp_log(str) {
	str = getDateTime() + ' ' + str + '\n';
	fs.appendFile('ssp.log', str, function (err) {
		//
	});
}

function getDateTime() {
    var date = new Date();
    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;
    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;
    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;
    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;
    return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
}

// SSP processing (Интерфейс купюроприемника NV9USB)
ssp = new ssp({
	device : 'COM7', //device address
	type : "nv9usb", //device type
	currencies : [1, 1, 1, 1, 1, 1]
});
ssp.init(function () {
	ssp.on('ready', function () {
		console.log("Device is ready");
		ssp.enable();
	});
	ssp.on('read_note', function (note) {
		if (note > 0) {
			console.log("GOT", notes[note]);
		}
	});
	ssp.on('disable', function () {
		console.log("disabled");
	});
	ssp.on('note_cleared_from_front', function (note) {
		console.log("note_cleared_from_front");
	});
	ssp.on('note_cleared_to_cashbox', function (note) {
		console.log("note_cleared_to_cashbox");
	});
	ssp.on('credit_note', function (note) {
		console.log("CREDIT", notes[note]);
		add_credit(note);
	});
	ssp.on("safe_note_jam", function (note) {
		console.log("Jammed", note);
		//TODO: some notifiaction, recording, etc.
	});
	ssp.on("unsafe_note_jam", function (note) {
		console.log("Jammed inside", note);
		//TODO: some notifiaction, recording, etc.
	});
	ssp.on("fraud_attempt", function (note) {
		console.log("Fraud!", note);
		//TODO: some notifiaction, recording, etc.
	});
	ssp.on("stacker_full", function (note) {
		console.log("I'm full, do something!");
		ssp.disable();
		//TODO: some notifiaction, recording, etc.
	});
	ssp.on("note_rejected", function (reason) {
		console.log("Rejected!", reason);
	});
	ssp.on("error", function (err) {
		console.log(err.code, err.message);
	});
});

// Startup
lock_interface();
