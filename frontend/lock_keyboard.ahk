﻿;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; THE KEYBOARD LOCKER                                       ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; This script will disable the keyboard when the user       ;
; presses Ctrl+Alt+L. The keyboard is reenabled if the user ;
; types in the string "unlock".                             ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Originally written by Lexikos:                            ;
;  http://www.autohotkey.com/forum/post-147849.html#147849  ;
; Modifications by Trevor Bekolay for the How-To Geek       ;
;  http://www.howtogeek.com/                                ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#Persistent

Process, WaitClose, HyperLaunch.exe, 4
Run, C:\HyperSpin\HyperSpin.exe
WinWait, HyperSpin

BlockKeyboard(true)

BlockKeyboard(block=-1) ; -1, true or false.
{
  static hHook = 0, cb = 0
  
  if !cb ; register callback once only.
    cb := RegisterCallback("BlockKeyboard_HookProc")
 
  if (block = -1) ; toggle
    block := (hHook=0)
	
  if ((hHook!=0) = (block!=0)) ; already (un)blocked, no action necessary.
    return
	
  if (block) {
    hHook := DllCall("SetWindowsHookEx"
      , "int", 13  ; WH_KEYBOARD_LL
      , "uint", cb ; lpfn (callback)
      , "uint", 0  ; hMod (NULL)
      , "uint", 0) ; dwThreadId (all threads)
  }
  else {
    DllCall("UnhookWindowsHookEx", "uint", hHook)
    hHook = 0
  }
}

BlockKeyboard_HookProc(nCode, wParam, lParam)
{
    static count = 0
    
    ; Unlock keyboard if "unlock" typed in
    if (NumGet(lParam+8) & 0x80) { ; key up
      if (count = 0 && NumGet(lParam+4) = 0x16) {        ; 'u'
        count = 1
      } else if (count = 1 && NumGet(lParam+4) = 0x31) { ; 'n'
        count = 2
      } else if (count = 2 && NumGet(lParam+4) = 0x26) { ; 'l'
        count = 3
      } else if (count = 3 && NumGet(lParam+4) = 0x18) { ; 'o'
        count = 4
      } else if (count = 4 && NumGet(lParam+4) = 0x2E) { ; 'c'
        count = 5
      } else if (count = 5 && NumGet(lParam+4) = 0x25) { ; 'k'
        count = 0
        BlockKeyboard(false)
      } else {
        count = 0
      }
    }

    return 1
}